import { Sides } from '../lib/constants/Sides';
import Peon from "../lib/battalions/Peon";
import Knight from '../lib/battalions/Knight';
import Horseman from '../lib/battalions/Horseman';
import Archer from '../lib/battalions/Archer';
import Battalion from '../lib/interfaces/Battalion';

test('get units values', () => {
  let units: Battalion[] = [
    new Peon(Sides.attack),
    new Knight(Sides.attack),
    new Horseman(Sides.attack),
    new Archer(Sides.attack)
  ]

  for (const unit of units) {
    console.log(`${unit.displayType} force is ${unit.battalionValue}`);
    expect(unit.battalionValue).toBeGreaterThan(0);
  }

});

