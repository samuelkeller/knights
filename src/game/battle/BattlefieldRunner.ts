import IBattlefieldReport from "../../lib/classes/IBattlefieldReport";
import BattlefieldReportStatus from "../../lib/constants/BattlefieldReportStatus";
import { Sides } from "../../lib/constants/Sides";
import { sleep, stringCompare } from "../../lib/Functions";
import Battalion from "../../lib/interfaces/Battalion";
import BattlefieldDrawer from "./BattlefieldDrawer"

const baseRange = 40;

export default class BattlefieldRunner extends BattlefieldDrawer {
    private attackers: Battalion[];
    private defenders: Battalion[];

    constructor(elem: HTMLElement, attackers: Battalion[], defenders: Battalion[]) {
        super(elem)
        this.attackers = attackers;
        this.defenders = defenders;
    }

    private get allUnits(): Battalion[] {
        return this.attackers.concat(this.defenders)
    }

    private get randomUnit(): Battalion {
        return this.allUnits[Math.floor(Math.random() * this.allUnits.length)];

    }

    async runBattle(): Promise<IBattlefieldReport> {
        this.initDrawBattle(this.attackers, this.defenders);
        const t0 = performance.now()
        let status = await this.battleLoop()
        const t1 = performance.now()
        return { status: status, time: t1 - t0 }
    }
    private async battleLoop(): Promise<string> {
        while (this.defenders.length > 0 && this.attackers.length > 0) {
            // Sélectionne une unité aléatoire qui va jouer.
            let selectedUnit: Battalion = this.randomUnit;

            let toCheck: Battalion[] = this.allUnits.filter(u => !stringCompare(selectedUnit.side, u.side));
            let closestUnit: { unit: Battalion, distance: number } = this.getClosestUnit(toCheck, selectedUnit);

            // Si on est en range d'une autre unité : attaque.
            if (closestUnit.distance < baseRange + baseRange * selectedUnit.stats.range) {
                // Ajout des échecs critiques.
                let miss = Math.random() > selectedUnit.stats.accuracy;

                // Si échec critique, continue la boucle.
                if (miss) {
                    selectedUnit.showMissAnimation(this.parentElement);
                    continue;
                }

                selectedUnit.attack(closestUnit.unit);

                // Mort d'unité, on l'enlève des différents choix possibles
                let status: string | null = null;
                if (closestUnit.unit.stats.health <= 0) {
                    status = this.handleUnitDeath(closestUnit.unit)
                } else if (selectedUnit.stats.health <= 0) {
                    status = this.handleUnitDeath(selectedUnit)
                }
                // Une des deux équipes à gagnéee
                if (status !== null)
                    return status
                selectedUnit.showAttackAnimation(this.parentElement);
            } else {
                selectedUnit = this.moveToClosest(selectedUnit, closestUnit.unit);
            }
            this.drawBalanceBar(
                this.attackers.map(a => a.stats.health).reduce((a, b) => a + b),
                this.defenders.map(a => a.stats.health).reduce((a, b) => a + b))
            await sleep(100)
        }
        return "";
    }

    private handleUnitDeath(unit: Battalion) {
        this.removeFromBattlefield(unit);
        // Après mort d'unité => vérification fin de partie.
        if (this.attackers.length === 0)
            return BattlefieldReportStatus.DEFENSE_WON;
        if (this.defenders.length === 0)
            return BattlefieldReportStatus.ATTACK_WON;
        return null
    }

    /**
     * Supprime une unité du champ de bataille.
    * @param closest unité
    */
    private removeFromBattlefield(closest: Battalion) {
        if (closest.side === Sides.defense) {
            let defsCopy = [...this.defenders];
            defsCopy.splice(defsCopy.findIndex(d => d._id === closest._id), 1);
            this.defenders = defsCopy;
        }
        else {
            let atkCopy = [...this.attackers];
            atkCopy.splice(atkCopy.findIndex(d => d._id === closest._id), 1);
            this.attackers = atkCopy;
        }
    }
}