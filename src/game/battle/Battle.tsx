import React from 'react';
import Battlefield from './components/Battlefield';
import "../../assets/css/battle.scss"
import { useStore } from 'react-redux';
import IPlayerSlice from '../../lib/interfaces/IPlayerSlice';
import { useHistory } from 'react-router-dom';
import { calcDefenders } from './BattleCalculators';

function Battle() {
  let history = useHistory();

  const store = useStore();
  let player = store.getState().player as IPlayerSlice

  let defenders = calcDefenders(player.stage);
  // Impossible d'accéder à une bataille sans unitées.
  let err = player.army.length === 0 || defenders.length === 0

  return (
    <div className="battle centered">
      {
        err ? <><button onClick={() => history.push("/")}>Erreur: retour à l'accueil</button></> : <>
          <Battlefield attackers={player.army} defenders={defenders} />
          <div id="battleInfos">
            <div id="balance-bar-container"></div>
            <span className="left blue">ATTACKERS</span><span className="right red">DEFENDERS</span>
          </div>
        </>

      }
    </div>
  );
}

export default Battle;
