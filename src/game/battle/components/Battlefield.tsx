import React, { useEffect } from 'react';
import Battalion from "../../../lib/interfaces/Battalion"
import BattlefieldRunner from "../BattlefieldRunner"

interface BattlefieldProps {
  attackers: Battalion[],
  defenders: Battalion[]
}

function Battlefield({ attackers, defenders }: BattlefieldProps) {
  function runBattle() {
    let elem = document.getElementById("battlefield");
    if (elem) {
      new BattlefieldRunner(elem, attackers, defenders).runBattle().then(res => {
        console.log(res)
      })
    }
  };

  useEffect(() => {
    runBattle()
  })

  return (
    <div id="battlefield">
    </div>
  );
}

export default Battlefield;
