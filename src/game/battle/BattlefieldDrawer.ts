import { getRandomInterval } from "../../lib/Functions";
import Battalion from "../../lib/interfaces/Battalion";
import { calcDistance } from "../../lib/Functions";

export default class BattlefieldDrawer {
    public parentElement: HTMLElement;
    private attackersSide: { max: number, min: number };
    private defendersSide: { max: number, min: number };
    private middleY: number

    constructor(parentElement: HTMLElement) {
        this.parentElement = parentElement;

        this.middleY = parentElement.offsetHeight / 2;
        let top = parentElement.offsetTop

        this.defendersSide = {
            min: top + this.middleY * 0.2,
            max: top + this.middleY * 0.8
        }

        this.attackersSide = {
            min: top + this.middleY + this.middleY * 0.2,
            max: top + this.middleY + this.middleY * 0.8
        }
    }

    public moveToClosest(unit: Battalion, closest: Battalion) {
        let xMove = unit.posX - closest.posX;
        let yMove = unit.posY - closest.posY;

        unit.move(Math.sign(xMove) * -5, Math.sign(yMove) * -5, this.parentElement, 5);
        return unit;
    }

    /**
     * Parcours un tableau d'unité et détermine celle étant le plus proche par ses
     * coordonnées.
     * @param toCheck Nombre d'unité dont les coordonnées devront être vérifiées
     * @param unit Unité de départ
     */
    public getClosestUnit(toCheck: Battalion[], unit: Battalion): { unit: Battalion, distance: number } {
        let closestDistance: number = 10000;
        let closest: Battalion | null = null;
        for (let i = 0; i < toCheck.length; i++) {
            let distance = calcDistance(unit.posX, unit.posY, toCheck[i].posX, toCheck[i].posY);
            if (distance < closestDistance) {
                closestDistance = distance;
                closest = toCheck[i];
            }
        }
        if (!closest)
            throw new Error("Aucune unité proche.");
        return { unit: closest, distance: closestDistance };
    }


    initDrawBattle(attackers: Battalion[], defenders: Battalion[]) {
        this.parentElement.innerHTML = "";

        // Distribution des unités en ligne
        let defendersXDistributions = getRandomInterval(this.parentElement.offsetLeft, this.parentElement.offsetLeft + this.parentElement.clientWidth, defenders.length);
        let attackersXDistributions = getRandomInterval(this.parentElement.offsetLeft, this.parentElement.offsetLeft + this.parentElement.clientWidth, attackers.length);

        // On place les défenseurs en haut
        for (const [i, defender] of defenders.entries()) {
            let x = defendersXDistributions[i];
            let y = Math.floor(Math.random() * (this.defendersSide.max - this.defendersSide.min + 1) + this.defendersSide.min);
            defender.place(x, y, this.parentElement);
        }

        // Et les attaquants en haut
        for (const [i, attacker] of attackers.entries()) {
            let x = attackersXDistributions[i];
            let y = Math.floor(Math.random() * (this.attackersSide.max - this.attackersSide.min + 1) + this.attackersSide.min);
            attacker.place(x, y, this.parentElement);
        }
    }

    drawBalanceBar(vAttack: number, vDefense: number) {
        if (vAttack < 0 || vDefense < 0 || vAttack > 100 || vDefense > 100)
            console.error("Incoherent values")
        let infos = document.getElementById("balance-bar-container");

        if (infos)
            infos.innerHTML = "";

        let attackBar = document.createElement("div");
        attackBar.style.width = vAttack / (vAttack + vDefense) * 100 + "%"
        attackBar.className = "balance-bar bg-blue white"
        attackBar.innerText = Math.round(vAttack).toString()

        let defenseBar = document.createElement("div");
        defenseBar.style.width = vDefense / (vAttack + vDefense) * 100 + "%"
        defenseBar.className = "balance-bar bg-red white"
        defenseBar.innerText = Math.round(vDefense).toString()


        infos?.appendChild(attackBar);
        infos?.appendChild(defenseBar);
    }
}