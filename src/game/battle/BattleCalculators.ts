import { Sides } from "../../lib/constants/Sides";
import Archer from "../../lib/battalions/Archer";
import Peon from "../../lib/battalions/Peon";
import Horseman from "../../lib/battalions/Horseman";
import Knight from "../../lib/battalions/Knight";
import Battalion from "../../lib/interfaces/Battalion";

/**
 * Traduit le niveau du joueur en unités qu'il doit combattre.
 * TODO: Difficultées ?
 * @param level Niveau du joueur.
 */
export const calcDefenders = (level: number) => {
    let units: Battalion[] = [
        new Peon(Sides.defense),
        new Knight(Sides.defense),
        new Horseman(Sides.defense),
        new Archer(Sides.defense)
    ]

    let defenders: Battalion[] = []
    let defendersLevel = 0;

    // Boucle, ajoute des unités jusqu'à arriver au niveau du joueur.
    while (level > defendersLevel) {
        let randUnit: Battalion = units.splice(Math.floor(Math.random() * units.length), 1)[0];
        defenders.push(randUnit)
        defendersLevel = defenders.map(a => a.stats.battalionValue).reduce((a, b) => a + b)
    }

    return defenders;
}