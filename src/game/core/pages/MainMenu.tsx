import React from 'react';
import "../../../assets/css/menu.scss"
import { useHistory } from "react-router-dom";



function MainMenu() {
  let history = useHistory();

  function unitChoiceRoute() {
    history.push("/unitChoice");
  }
  return (
    <div className="main-menu centered">
      <header className="menu-header">
        <p>Knights
        </p>
      </header>
      <section className="menu-actions">
        <button onClick={unitChoiceRoute}>Démarrer</button>
        <button onClick={() => console.log("yes")}>High scores</button>
      </section>
    </div>
  );
}

export default MainMenu;
