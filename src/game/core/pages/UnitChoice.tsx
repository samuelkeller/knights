import React from 'react';
import "../../../assets/css/menu.scss"
import Archer from '../../../lib/battalions/Archer';
import Knight from '../../../lib/battalions/Knight';
import Horseman from '../../../lib/battalions/Horseman';
import Battalion from '../../../lib/interfaces/Battalion';
import { connect, useDispatch } from 'react-redux';
import { setBattalions } from '../../../store/player-slice';
import { useHistory } from 'react-router-dom';
import { Sides } from '../../../lib/constants/Sides';

connect()

function UnitChoice() {
  let dispatch = useDispatch()
  let history = useHistory();

  let choices = [
    new Knight(Sides.attack),
    new Archer(Sides.attack),
    new Horseman(Sides.attack)
  ]
  const selectStarter = (bat: Battalion) => {
    dispatch(setBattalions([bat]))
    history.push("/battle")
  }

  return (
    <section id="choices-container" className="centered">
      <h1 className="white">Avec qui allez-vous concourir au tournoi des 18 chevaliers blblbl ?</h1>
      <ul className="choices-list">
        {choices.map(choice => {
          return (
            <li key={choice._id} className="choice" onClick={() => selectStarter(choice)}>
              <h2 className="white">{choice.displayType}</h2>
              <img src={choice.spriteLg} alt={choice.type} />
            </li>
          )
        })}
      </ul>
    </section>
  );
}

export default UnitChoice;
