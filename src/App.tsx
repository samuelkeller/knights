import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import './assets/css/css-reset.css';
import './assets/css/index.scss';
import Battle from './game/battle/Battle';
import MainMenu from "./game/core/pages/MainMenu"
import UnitChoice from "./game/core/pages/UnitChoice"

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact>
            <MainMenu />
          </Route>
          <Route path="/battle" exact>
            <Battle />
          </Route>
          <Route path="/unitChoice" exact>
            <UnitChoice />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
