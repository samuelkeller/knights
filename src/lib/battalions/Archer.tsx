import Battalion from "../interfaces/Battalion"
import batallionTypes from "../constants/batallionTypes";

const type = batallionTypes.archer;

export default class Archer extends Battalion {
    constructor(side: string) {
        super(type, side);

    }
}