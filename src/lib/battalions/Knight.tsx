import Battalion from "../interfaces/Battalion"
import batallionTypes from "../constants/batallionTypes";

const type = batallionTypes.knight;

export default class Knight extends Battalion {
    constructor(side: string) {
        super(type, side);
    }
}