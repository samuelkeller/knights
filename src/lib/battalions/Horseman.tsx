import Battalion from "../interfaces/Battalion"
import batallionTypes from "../constants/batallionTypes";

const type = batallionTypes.horseman;

export default class Horseman extends Battalion {
    constructor(side: string) {
        super(type, side);
    }
}