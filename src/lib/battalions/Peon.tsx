import Battalion from "../interfaces/Battalion"
import batallionTypes from "../constants/batallionTypes";

const type = batallionTypes.peon;

export default class Horseman extends Battalion {
    constructor(side: string) {
        super(type, side);
    }
}