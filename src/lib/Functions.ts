
/**
 * Reconstruit un interval de points sur un segment (n points équidistants);
 * @param min Minimum que puisse avoir un point
 * @param max Maximum qui puisse avoir un point
 * @param points Nombre de points
 */
export function getRandomInterval(min, max, points) {
    let interval = (max - min) / points;
    let arr: number[] = [];

    for (let i = 0; i < points; i++) {
        arr[i] = min + (interval * i) + (min * 0.5)
    }
    return arr
}

// Calcule la distance entre deux points
export function calcDistance(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

export const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

/**
 * Renvoie true si les strings sont les mêmes sinon false
 * @param a 
 * @param b 
 */
export function stringCompare(a: string, b: string) {
    if (a.toString() < b.toString()) return false;
    if (a.toString() > b.toString()) return false;
    return true;
}

export function getRandomElementInArray<T>(items: Array<T>): T {
    return items[Math.floor(Math.random() * items.length)];
}