import Placable from "../classes/Placable"
import { Sides } from "../constants/Sides"
import { stringCompare } from "../Functions";
import { getNameByType } from "../../lib/NameGenerator"
import BattalionStats from "../classes/BattalionStats"

const iconSize = 32;

interface Battalion extends Placable { }

abstract class Battalion extends Placable {
    public type: string;
    public side: string;
    public name: string;
    public stats: BattalionStats

    constructor(type: string, side: string) {
        let border = stringCompare(side, Sides.attack) ? "blue" : "red";
        super(type, iconSize, iconSize, border)
        this.side = side;
        this.type = type;
        this.stats = new BattalionStats(type)
        this.name = getNameByType(type);
    }

    // Renvoie une version affichable du type de l'unité
    // Pour l'instant, met en majuscule la première lettre.
    public get displayType(): string {
        return this.type.charAt(0).toUpperCase() + this.type.slice(1)
    }

    attack(unit: Battalion): void {
        let updatedHealth = Math.round(unit.stats.health -= this.stats.attack);
        if (updatedHealth <= 0)
            unit.remove();
        else {
            unit.stats.health = updatedHealth
            unit.refreshHealthBar(updatedHealth)
        }
    }
}

export default Battalion;