import Battalion from "./Battalion"

export default interface IPlayerSlice {
    stage: number,
    army: Battalion[]
}