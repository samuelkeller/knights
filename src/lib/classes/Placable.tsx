const lgImagesSize = 256;

export default class Placable {
    public _id: string;
    public posX: number
    public posY: number
    private sizeX: number
    private sizeY: number
    private sprite: string;
    public spriteLg: string;
    private border: string;
    private displayHealth: number;

    constructor(type: string, sizeX: number, sizeY: number, border: string) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.posX = 0;
        this.posY = 0;
        this.border = border;
        this.displayHealth = 100;
        this.sprite = require('../../assets/battle/battalions/' + type + '-' + sizeX + 'x' + sizeY + '.png');
        this.spriteLg = require('../../assets/battle/battalions/' + type + '-' + lgImagesSize + 'x' + lgImagesSize + '.png');
        this._id = '_' + Math.random().toString(36).substr(2, 9);;
    }

    move(xMove: number, yMove: number, parentElement: HTMLElement, frames: number) {
        for (let i = 0; i < frames; i++) {
            this.place(this.posX + xMove / frames, this.posY + yMove / frames, parentElement)
        }
    }

    place(posX: number, posY: number, parentElement: HTMLElement): HTMLImageElement {
        this.remove();

        this.posX = posX;
        this.posY = posY;

        // Traitement HTML
        let placableDiv = document.createElement("div");
        placableDiv.style.border = `2px dotted ${this.border}`
        placableDiv.style.width = this.sizeX.toString() + 'px'
        placableDiv.style.position = "absolute";
        placableDiv.style.left = this.posX.toString() + 'px';
        placableDiv.style.top = this.posY.toString() + 'px';
        placableDiv.id = this._id;

        // On initie la barre de vie;
        let healthBar = document.createElement("progress")
        healthBar.id = this.getHealthBarId();
        healthBar.max = 100;
        healthBar.value = this.displayHealth;
        healthBar.style.width = "100%"
        placableDiv.appendChild(healthBar);

        // Icone du placable
        let placable = document.createElement("img");
        placable.src = this.sprite
        parentElement.appendChild(placableDiv);
        placableDiv.appendChild(placable);

        return placable
    }

    remove() {
        let elem = document.getElementById(this._id);
        elem?.parentNode?.removeChild(elem);
    }

    showMissAnimation(parentElement: HTMLElement) {
        let attackIcon = document.createElement("img");
        attackIcon.src = require("../../assets/battle/miss.png")
        this.showAnimation(attackIcon, parentElement);
    }
    showAttackAnimation(parentElement: HTMLElement) {
        // On initie la barre de vie;
        let attackIcon = document.createElement("img");
        attackIcon.src = require("../../assets/battle/swords.png")
        this.showAnimation(attackIcon, parentElement);
    }

    private showAnimation(attackIcon: HTMLImageElement, parentElement: HTMLElement) {
        attackIcon.style.position = "absolute";
        attackIcon.style.left = this.posX.toString() + 'px';
        attackIcon.style.top = this.posY.toString() + 'px';
        parentElement.appendChild(attackIcon);
        setTimeout(() => {
            parentElement.removeChild(attackIcon);
        }, 100);
    }

    refreshHealthBar(health: number) {
        if (health < 0)
            throw new Error("Barre de vie négative.")
        this.displayHealth = health;
        let elem: HTMLProgressElement = document.getElementById(this.getHealthBarId()) as HTMLProgressElement
        if (elem)
            elem.value = health;
    }

    private getHealthBarId = () => {
        return this._id + "-bar"
    }
}