import { batallionStats } from "../constants/batallionStats";


export default class BattalionStats {
    public experience: number;
    public health: number;
    public attack!: number;
    public accuracy!: number;

    // Distance à laquelle une unité peut faire des dégats
    public range!: number;

    constructor(type: string) {
        this.experience = 0;
        this.health = 100;

        // Ajout données depuis dictionnaire.
        // TODO BDD
        this.attack = batallionStats[type].attack;
        this.accuracy = batallionStats[type].accuracy;
        this.range = batallionStats[type].range;
    }

    public get level() {
        return Math.round(Math.sqrt(this.experience))
    }


    // Renvoie une valeur relative arbitraite de l'unité.
    // Cela permet d'"équilibrer" les combats.
    public get battalionValue(): number {
        return Math.floor(((this.attack * this.accuracy) + this.range) * 100) / 100
    }
}