interface IStat {
    attack: number,
    accuracy: number,
    attackSpeed: number,
    range: number,
}
interface IBattalionStat {
    [key: string]: IStat;
}

export const batallionStats: IBattalionStat = {
    archer: {
        attack: 0.8,
        accuracy: 0.8,
        attackSpeed: 1.3,
        range: 4
    },
    knight: {
        attack: 1.2,
        accuracy: 0.9,
        attackSpeed: 1,
        range: 1
    },
    horseman: {
        attack: 1.7,
        accuracy: 0.6,
        attackSpeed: 0.8,
        range: 2
    },
    peon: {
        attack: 0.3,
        accuracy: 0.3,
        attackSpeed: 0.3,
        range: 1
    }
}
