import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { playerReducer } from './player-slice';


// The only thing that works is, indeed, using type RootState = ReturnType<typeof store.getState> and passing the reducers directly 
// to configureStore(). combineReducers() would still turn the type into unknown.
export const store = configureStore({
    reducer: {
        player: playerReducer
    },
    middleware: getDefaultMiddleware({
        serializableCheck: false
    }),
});

export type RootState = ReturnType<typeof store.getState>