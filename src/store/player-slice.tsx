const { createSlice } = require("@reduxjs/toolkit");

const playerSlice = createSlice({
    name: "player",
    initialState: {
        stage: 1,
        army: []
    },
    reducers: {
        addBattalion(state, { payload }) {
            state.army.push(payload)
            return state;
        },
        setBattalions(state, { payload }) {
            state.army = payload
            return state;
        },
        upgradeStage(state) {
            state.stage++;
            return state;
        }
    }
})

export const playState = (state) => { return state.player }
export const playerReducer = playerSlice.reducer;
export const { addBattalion, setBattalions, upgradeStage } = playerSlice.actions;